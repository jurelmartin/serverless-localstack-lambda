# serverless-localstack-lambda
Serverless LocalStack Lambda


# Using VS Code Devcontainer

Go to [Wiki - Setup Guide](https://gitlab.com/devcontainers2/serverless-localstack-lambda/-/wikis/Setup-Guide)

# Update log

As of 08-12-2022
* no need to update the localstack IP; localstack is now accessible directly inside container via http://localstack:4566.
